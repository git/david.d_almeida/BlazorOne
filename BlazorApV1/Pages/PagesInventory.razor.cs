﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using BlazorApV1.Services;
using BlazorApV1.Models;
using BlazorApV1.Modals;
using System.Globalization;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Microsoft.Extensions.Localization;


namespace BlazorApV1.Pages
{
    public partial class PagesInventory
    {
        [Inject]
        public IDataService DataService { get; set; }


        [Inject]
        public IStringLocalizer<List> Localizer { get; set; }


        private string? title;
        public List<Item> Items { get; set; } = new List<Item>();

        private int totalItem;


        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }
            Items = await DataService.List(0, await DataService.Count());

            StateHasChanged();
        }
    }
}
