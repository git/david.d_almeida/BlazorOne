﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;


[Route("[controller]/[action]")]
    public class CultureController : Controller
    {
        
        public IActionResult SetCulture(string culture, string redirectUri)
        {
            if (culture != null)
            {
                // Define a cookie with the selected culture
                this.HttpContext.Response.Cookies.Append(
                    CookieRequestCultureProvider.DefaultCookieName,
                    CookieRequestCultureProvider.MakeCookieValue(
                        new RequestCulture(culture)));
            }

            return this.LocalRedirect(redirectUri);
        }
    }

