﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using BlazorApV1.Weather;
using BlazorApV1.Models;
using BlazorApV1.Pages;
using System.Linq;
using System.Threading.Tasks;
using BlazorApV1.Services;
using System.Text.Json;
using Microsoft.JSInterop;
using Blazorise.DataGrid;
using Blazorise;
using Microsoft.AspNetCore.Components;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace BlazorApV1.Components
{
    public partial class Inventory
    {
        [CascadingParameter]
        public Inventory Director { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public bool IsntDrop { get; set; }


        internal void OnDragEnter()
        {
            if (IsntDrop)
            {
                return;
            }

            Director.Actions.Add(new InventoryAction { Action = "Drag Enter", Item = this.Item, Index = this.Index });
        }

        internal void OnDragLeave()
        {
            if (IsntDrop)
            {
                return;
            }

            Director.Actions.Add(new InventoryAction { Action = "Drag Leave", Item = this.Item, Index = this.Index });
        }
        internal void OnDrop()
        {

            if (IsntDrop == true || Director.CurrentDragItem == null)
            {
                return;
            }
            if (this.Item == null)
            {
                this.Item = ItemFactory.Create(Director.CurrentDragItem);
                Director.RecipeItems[this.Index] = this.Item;
                Director.Actions.Add(new InventoryAction { Action = "Drop", Item = this.Item, Index = this.Index });
                if (Director.CurrentDragItemIndex != -1)
                {
                    Director.CurrentDragItem = null;
                    Director.RecipeItems[Director.CurrentDragItemIndex] = null;
                    Director.Actions.Add(new InventoryAction { Action = "Moove", Item = this.Item, Index = this.Index });
                }

            }
            else if (this.Item.Id == Director.CurrentDragItem.Id && Director.CurrentDragItemIndex != this.Index)
            {
                if (this.Item.StackSize > this.Item.Num)
                {
                    ItemFactory.Add(this.Item, Director.CurrentDragItem);
                    Director.RecipeItems[this.Index] = this.Item;
                    Director.Actions.Add(new InventoryAction { Action = "Drop", Item = this.Item, Index = this.Index });
                    Director.Actions.Add(new InventoryAction { Action = "Moove", Item = this.Item, Index = this.Index });
                }

            }
            else if (Director.CurrentDragItemIndex != -1)
            {
                Director.RecipeItems[Director.CurrentDragItemIndex] = this.Item;
                this.Item = ItemFactory.Create(Director.CurrentDragItem);
                Director.RecipeItems[this.Index] = this.Item;

                Director.Actions.Add(new InventoryAction { Action = "Swap Position", Item = this.Item, Index = this.Index });
            }

            Director.RecipeItems[this.Index] = this.Item;
            string fileName = "Stack.json";
            string jsonString = JsonSerializer.Serialize(Director.RecipeItems);
            File.WriteAllText(fileName, jsonString);
        }
        

        private void OnDragStart()
        {

            Director.CurrentDragItem = this.Item;
            Director.CurrentDragItemIndex = this.Index;
            Director.Actions.Add(new InventoryAction { Action = "Drag Start", Item = this.Item, Index = this.Index });
        }
       


        private void OnDragEnd()
        {
            if (Director.Actions.Last().Action == "Drag Leave" && Director.CurrentDragItemIndex != -1 && this.Item != null)
            {
                this.Item = null;
                Director.Actions.Add(new InventoryAction { Action = "Delete", Item = this.Item, Index = this.Index });

                Director.RecipeItems[this.Index] = null;
                string fileName = "End.json";
                string jsonString = JsonSerializer.Serialize(Director.RecipeItems);
                File.WriteAllText(fileName, jsonString);
            }

            if (Director.Actions.Last().Action == "Moove" && Director.CurrentDragItemIndex != -1)
            {
                this.Item = null;
                Director.RecipeItems[this.Index] = null;
                string fileName = "Stack.json";
                string jsonString = JsonSerializer.Serialize(Director.RecipeItems);
                File.WriteAllText(fileName, jsonString);
            }

            if (Director.Actions.Last().Action == "Swap Position")
            {
                this.Item = Director.RecipeItems[this.Index];
            }

        }
    }
}
